﻿$.fn.dataTable.ext.search.push(function (settings, data, dataIndex) {
    if (settings.sTableId == "RawDataTable") {
        var EVvalueText = document.getElementById("EV").value;
        var chkBoxMA = document.getElementById("chkboxMA");
        var chkBoxFF = document.getElementById("chkboxFF");

        if (chkBoxMA.checked) {
            var value = data[12].toString().replace("%", "").replace(",", ".");
            var MA = parseFloat(value);

            if (MA < 0 | value == "") {
                return false;
            }
        }

        if (chkBoxFF.checked) {
            var value = data[2].toString().replace("%", "").replace(",", ".");
            var FF = value.toString();

            if (FF == "Finans & Fastighet") {
                return false;
            }
        }

        if (EVvalueText != "") {
            var EV = parseFloat(data[3]) || 0;
            var EVvalue = parseFloat(EVvalueText);

            if (EVvalue > EV) {
                return false;
            }
        }

        return true;
    }

    if (settings.sTableId == "RankDataTable") {
        return true;
    }
    if (settings.sTableId == "EVBITDataTable") {
        return true;
    }
});

$("#filterRawData").click(function () {
    var table = $('#RawDataTable').DataTable();
    table.draw();
});

$("#Send").click(function () {
    if ($('#RawDataTable').DataTable().rows().count() > 0) {
        var jsonObject = JSON.stringify(tableToJSON($("#RawDataTable")));
        var countVC = document.getElementById("countVC").value;
        var countEBIT = document.getElementById("countEBIT").value;

        $('#collapseRaw').collapse('toggle');

        $.ajax({
            type: 'POST',
            url: 'Home/PostDatatable',
            data: { 'saniData': JSON.stringify(jsonObject), 'count': countVC, 'responsType': 0 },
            success: function (data) {
                var response = jQuery.parseJSON(data);
                var table = $('#RankDataTable').DataTable();

                table.clear().draw();
                table.rows.add(response).draw();

                $('#collapseRank').collapse('show');
            }
        });

        $.ajax({
            type: 'POST',
            url: 'Home/PostDatatable',
            data: { 'saniData': JSON.stringify(jsonObject), 'count': countEBIT, 'responsType': 1, 'EBITratio': sliderEBIT.value },
            success: function (data) {
                var response = jQuery.parseJSON(data);
                var table = $('#EVBITDataTable').DataTable();

                table.clear().draw();
                table.rows.add(response).draw();
            }
        });
    }
});

//Update the current slider value (each time you drag the slider handle)
sliderVC.oninput = function () {
    outputVC.innerHTML = "VC: " + sliderVC.value + "% | Momentum: " + (100 - sliderVC.value) + "%";
}

sliderEBIT.oninput = function () {
    outputEBIT.innerHTML = "Hämta ut topp: " + sliderEBIT.value + "% med högst EV/EBIT";
}

function tableToJSON(tblObj) {
    var data = [];
    var $headers = $(tblObj).find("th");
    var $rows = $(tblObj).find("tbody tr").each(function (index) {
        $cells = $(this).find("td");
        data[index] = {};
        $cells.each(function (cellIndex) {
            data[index][$($headers[cellIndex]).html()] = $(this).html();
        });
    });
    return data;
}