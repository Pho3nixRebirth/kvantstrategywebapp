﻿var sliderVC = document.getElementById("myRangeVC");
var sliderEBIT = document.getElementById("myRangeEBIT");
var outputVC = document.getElementById("ratioLabelVC");
var outputEBIT = document.getElementById("ratioLabelEBIT");

$(document).ready(function () {
    var table = $('#RawDataTable').DataTable({
        "ordering": true, // false to disable sorting (or any other option)
        "paging": false,
        "bInfo": true,
        "dom": '<"top"if>'
    });
    $('.dataTables_length').addClass('bs-select');

    $('#RankDataTable').DataTable({
        "ordering": true, // false to disable sorting (or any other option)
        "paging": false,
        "bInfo": true,
        "dom": '<"top"i>',
        "columns": [
            { "title": "Bolagsnamn", "data": "Bolagsnamn" },
            { "title": "P/E", "data": "P/E" },
            { "title": "P/S", "data": "P/S" },
            { "title": "P/B", "data": "P/B" },
            { "title": "P/FCF", "data": "P/FCF" },
            { "title": "P/EBITDA", "data": "P/EBITDA" },
            { "title": "Momentum Rank", "data": "Momentum Rank" },
            { "title": "VC Rank", "data": "VC Rank" },
            { "title": "Total VC + MoM", "data": "Total VC + MoM" },
            { "title": "VC + MoM Rank", "data": "VC + MoM Rank" },
            {
                "title": "Del", "data": "Delete", 'render': function (data, type, full, meta) {
                    return '<img src="/images/remove.png" height="24" width="24">';
                }
            }
        ],
        "order": [[9, "asc"]],
        buttons: ['copy']
    });
    $('.dataTables_length').addClass('bs-select');

    $('#EVBITDataTable').DataTable({
        "ordering": true, // false to disable sorting (or any other option)
        "paging": false,
        "bInfo": true,
        "dom": '<"top"i>',
        "columns": [
            { "title": "Bolagsnamn", "data": "Bolagsnamn" },
            { "title": "EV/EBIT", "data": "EV/EBIT" },
            { "title": "Momentum", "data": "Momentum" },
            { "title": "Rank", "data": "Rank" },
            {
                "title": "Ta bort?", "data": "Delete", 'render': function (data, type, full, meta) {
                    return '<img src="/images/remove.png" height="24" width="24">';
                }
            }
        ],
        "columnDefs": [
            { "width": "20", "targets": "0" }
        ],
        "order": [[3, "asc"]],
        "bAutoWidth": false,
        "columnDefs": [
            { "width": "30%", "targets": 0 },
            { "width": "10%", "targets": 1 },
            { "width": "10%", "targets": 2 },
            { "width": "10%", "targets": 3 },
            { "width": "5%", "targets": 4 }
        ]
    });
    $('.dataTables_length').addClass('bs-select');

    if (table.rows().count() > 0) {
        $('#collapseRaw').collapse('show');
    }

    var EBITtable = $('#EVBITDataTable').DataTable();
    $('#EVBITDataTable tbody').on('click', '[src*=remove]', function () {
        EBITtable.row($(this).closest('tr')).remove().draw(false);
    });

    var VCtable = $('#RankDataTable').DataTable();
    $('#RankDataTable tbody').on('click', '[src*=remove]', function () {
        VCtable.row($(this).closest('tr')).remove().draw(false);
    });

    //Display the default slider value
    sliderEBIT.value = 20;
    outputVC.innerHTML = "VC: " + sliderVC.value + "% | Momentum: " + (100 - sliderVC.value) + "%";
    outputEBIT.innerHTML = "Hämta ut topp: " + sliderEBIT.value + "% med högst EV/EBIT";
});