﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using System.Data;
using OfficeOpenXml;
using KvantStrategi.Models;
using Newtonsoft.Json;
using System.ComponentModel;
using FastMember;
using Microsoft.AspNetCore.Http;
using OfficeOpenXml.Style;
using Microsoft.AspNetCore.Http.Internal;
using System.IO;

namespace KvantStrategi.Controllers
{
    public class HomeController : Controller
    {
        [HttpGet]
        public IActionResult Calculate()
        {
            return View(null);
        }

        [HttpPost]
        public IActionResult Calculate(IFormFile file = null)
        {
            if (file == null)
            {
                ViewData["ErrorMsg"] = "Välj en fil med rådata!";

                return View(null);
            }
            else
            {
                ExcelPackage verifiedFile = CheckAndVerifyExcelData(file);

                if (verifiedFile != null)
                {
                    DatatableObjects dataObject = new DatatableObjects();

                    DataTable rawDataTable = GetDataTableFromExcel(verifiedFile);
                    DataTable cleanData = CleanTheData(rawDataTable);
                    DataTable rankData = CreateEmptyResultTable(cleanData)[0];
                    DataTable EBITData = CreateEmptyResultTable(cleanData)[1];

                    dataObject.SanitizedDataTable = cleanData;
                    dataObject.RankedDataTable = rankData;
                    dataObject.RankedEBITDataTable = EBITData;

                    return View(dataObject);
                }

                ViewData["ErrorMsg"] = "Filen hade fel format!";

                return View(null);
            }
        }

        [HttpPost]
        [Route("Home/PostDatatable")]
        public string PostDatatable(string saniData, int responsType, int count = 10, int EBITratio = 20)
        {
            ResponsType type = (ResponsType)responsType;

            string json = saniData.Replace("\\", string.Empty);
            json = json.Trim('"');

            IEnumerable<DataTableJson> deserializedProduct = JsonConvert.DeserializeObject<IEnumerable<DataTableJson>>(json);

            DataTable table = new DataTable();
            using (var reader = ObjectReader.Create(deserializedProduct))
            {
                table.Load(reader);
            }

            string JsonString = "";
            switch (type)
            {
                case ResponsType.VC:
                    table.Columns["PE"].ColumnName = "P/E";
                    table.Columns["PS"].ColumnName = "P/S";
                    table.Columns["PB"].ColumnName = "P/B";
                    table.Columns["FCF"].ColumnName = "P/FCF";
                    table.Columns["EBITDA"].ColumnName = "P/EBITDA";
                    table.Columns["EBIT"].ColumnName = "EV/EBIT";
                    table.Columns["Mom12"].ColumnName = "Utveck.  1 år";
                    table.Columns["Mom6"].ColumnName = "Utveck.  6m";
                    table.Columns["Mom3"].ColumnName = "Utveck.  3m";
                    table.Columns["MA"].ColumnName = "Medelvärde";
                    table.Columns["Rapport"].ColumnName = "Sen. Rapport";

                    DataTable rankDataVC = ResultsViewVC(table);
                    var sorted2 = rankDataVC.Select().Where(x => Convert.ToInt32(x["VC + MoM Rank"]) <= count).CopyToDataTable();

                    JsonString = JsonConvert.SerializeObject(sorted2, Formatting.Indented);
                    break;
                case ResponsType.EBIT:
                    table.Columns["PE"].ColumnName = "P/E";
                    table.Columns["PS"].ColumnName = "P/S";
                    table.Columns["PB"].ColumnName = "P/B";
                    table.Columns["FCF"].ColumnName = "P/FCF";
                    table.Columns["EBITDA"].ColumnName = "P/EBITDA";
                    table.Columns["EBIT"].ColumnName = "EV/EBIT";
                    table.Columns["Mom12"].ColumnName = "Utveck.  1 år";
                    table.Columns["Mom6"].ColumnName = "Utveck.  6m";
                    table.Columns["Mom3"].ColumnName = "Utveck.  3m";
                    table.Columns["MA"].ColumnName = "Medelvärde";
                    table.Columns["Rapport"].ColumnName = "Sen. Rapport";

                    DataTable rankDataEBIT = ResultViewEBIT(table, EBITratio);
                    var sorted2Ebit = rankDataEBIT.Select().Where(x => Convert.ToInt32(x["Rank"]) <= count).CopyToDataTable();
                    //var sorted2Ebit = rankDataEBIT.Select().CopyToDataTable();

                    JsonString = JsonConvert.SerializeObject(sorted2Ebit, Formatting.Indented);
                    break;
                default:
                    break;
            }

            return JsonString;
        }

        private DataTable GetDataTableFromExcel(ExcelPackage file, bool hasHeader = true)
        {
            using (var pck = file)
            {
                var ws = pck.Workbook.Worksheets.First();
                DataTable tbl = new DataTable();
                foreach (var firstRowCell in ws.Cells[1, 1, 1, ws.Dimension.End.Column])
                {
                    if (tbl.Columns.Contains(firstRowCell.Text))
                    {
                        Console.WriteLine();
                    }
                    tbl.Columns.Add(hasHeader ? firstRowCell.Text : string.Format("Column {0}", firstRowCell.Start.Column));
                }
                var startRow = hasHeader ? 2 : 1;
                for (int rowNum = startRow; rowNum <= ws.Dimension.End.Row; rowNum++)
                {
                    ExcelRange wsRow = ws.Cells[rowNum, 1, rowNum, ws.Dimension.End.Column];
                    DataRow row = tbl.Rows.Add();
                    foreach (var cell in wsRow)
                    {
                        if (cell.RichText.Count > 1)
                        {
                            foreach (ExcelRichText richTextItem in cell.RichText)
                            {
                                if (richTextItem.Bold == true)
                                {

                                    row[cell.Start.Column - 1] += "<b>" + richTextItem.Text + "</b>";
                                }
                                else
                                {
                                    row[cell.Start.Column - 1] += richTextItem.Text;
                                }
                            }
                        }
                        else
                        {
                            row[cell.Start.Column - 1] = cell.Text;
                        }
                    }
                }

                return tbl;
            }
        }

        private ExcelPackage CheckAndVerifyExcelData(IFormFile file)
        {
            List<bool> VerifiedColumns = new List<bool>();
            var pck = new ExcelPackage();
            using (var stream = file.OpenReadStream())
            {
                pck.Load(stream);
            }

            var ws = pck.Workbook.Worksheets.First();
            ExcelRange wsRow2 = ws.Cells[2, 1, 2, ws.Dimension.End.Column];

            var findMom1 = ws.Cells[2, 1, 2, ws.Dimension.End.Column].Select(x => x.Text == "Utveck.  1 år");
            var findMom6 = ws.Cells[2, 1, 2, ws.Dimension.End.Column].Select(x => x.Text == "Utveck.  6m");
            var findLand = ws.Cells[2, 1, 2, ws.Dimension.End.Column].Select(x => x.Text == "Land");
            var findSector = ws.Cells[2, 1, 2, ws.Dimension.End.Column].Select(x => x.Text == "Sektor");
            var findInstrument = ws.Cells[2, 1, 2, ws.Dimension.End.Column].Select(x => x.Text == "Lista");

            if (findMom1.Contains(true))
            {
                for (int i = 0; i < findMom1.Count(); i++)
                {
                    if (findMom1.ToArray()[i] == true)
                    {
                        string newValue = wsRow2[2, i + 1].Text;
                        ws.Cells[1, i + 1].Value = newValue;

                        break;
                    }
                }
            }

            if (findMom6.Contains(true))
            {
                for (int i = 0; i < findMom6.Count(); i++)
                {
                    if (findMom6.ToArray()[i] == true)
                    {
                        string newValue = wsRow2[2, i + 1].Text;
                        ws.Cells[1, i + 1].Value = newValue;

                        break;
                    }
                }
            }

            if (findLand.Contains(true))
            {
                for (int i = 0; i < findLand.Count(); i++)
                {
                    if (findLand.ToArray()[i] == true)
                    {
                        string newValue = wsRow2[2, i + 1].Text;
                        ws.Cells[1, i + 1].Value = newValue;

                        break;
                    }
                }
            }

            if (findInstrument.Contains(true))
            {
                for (int i = 0; i < findInstrument.Count(); i++)
                {
                    if (findInstrument.ToArray()[i] == true)
                    {
                        string newValue = wsRow2[2, i + 1].Text;
                        ws.Cells[1, i + 1].Value = newValue;

                        break;
                    }
                }
            }

            if (findSector.Contains(true))
            {
                for (int i = 0; i < findSector.Count(); i++)
                {
                    if (findSector.ToArray()[i] == true)
                    {
                        string newValue = wsRow2[2, i + 1].Text;
                        ws.Cells[1, i + 1].Value = newValue;

                        break;
                    }
                }
            }

            ws.DeleteRow(2);
            pck.Save();

            ExcelRange wsRowVerify = ws.Cells[1, 1, 1, ws.Dimension.End.Column];
            ExcelRange wsRowVerify2 = ws.Cells[1, 1, 2, ws.Dimension.End.Column];

            var array = wsRowVerify.Value as object[,];
            var list = array.Cast<string>().ToList();

            VerifiedColumns.Add(list.Contains("Bolagsnamn"));
            VerifiedColumns.Add(list.Contains("Land"));
            VerifiedColumns.Add(list.Contains("Sector"));
            VerifiedColumns.Add(list.Contains("Börsvärde"));
            VerifiedColumns.Add(list.Contains("P/E"));
            VerifiedColumns.Add(list.Contains("P/S"));
            VerifiedColumns.Add(list.Contains("P/FCF"));
            VerifiedColumns.Add(list.Contains("P/EBITDA"));
            VerifiedColumns.Add(list.Contains("EV/EBIT"));
            VerifiedColumns.Add(list.Contains("Medelvärde"));
            VerifiedColumns.Add(list.Contains("Sen. Rapport"));
            VerifiedColumns.Add(list.Contains("Utveck.  1 år"));
            VerifiedColumns.Add(list.Contains("Utveck.  6m"));
            VerifiedColumns.Add(list.Contains("Lista"));

            bool verified = VerifiedColumns.Contains(true);

            if (verified)
            {
                var fileBytes = pck.GetAsByteArray();

                using (var ms = new MemoryStream(fileBytes))
                {
                    return pck;
                }
            }
            else
            {
                return null;
            }
        }

        private DataTable CleanTheData(DataTable rawData)
        {
            DataTable tbl = rawData.Clone();

            foreach (DataRow item in rawData.Rows)
            {
                DataRow sItem = SanitizeData(item, rawData);

                if (sItem != null)
                {
                    tbl.ImportRow(sItem);
                }
            }

            return tbl;
        }

        private DataRow SanitizeData(DataRow row, DataTable rawData)
        {
            for (int i = 8; i < 11; i++)
            {
                double.TryParse(row[i].ToString().Replace("%", ""), out double momentum);

                if (row[i].ToString() == "" | momentum < 0)
                {
                    return null;
                }
            }

            DataRow outputRow = row;
            var highestPE = rawData.AsEnumerable().Where(x => double.TryParse(x.Field<string>("P/E"), out double res) == true).Max(x => Convert.ToDouble(x.Field<string>("P/E")));
            var highestPS = rawData.AsEnumerable().Where(x => double.TryParse(x.Field<string>("P/S"), out double res) == true).Max(x => Convert.ToDouble(x.Field<string>("P/S")));
            var highestPB = rawData.AsEnumerable().Where(x => double.TryParse(x.Field<string>("P/B"), out double res) == true).Max(x => Convert.ToDouble(x.Field<string>("P/B")));
            var highestPFCF = rawData.AsEnumerable().Where(x => double.TryParse(x.Field<string>("P/FCF"), out double res) == true).Max(x => Convert.ToDouble(x.Field<string>("P/FCF")));
            var highestPEBITDA = rawData.AsEnumerable().Where(x => double.TryParse(x.Field<string>("P/EBITDA"), out double res) == true).Max(x => Convert.ToDouble(x.Field<string>("P/EBITDA")));
            var highestEBIT = rawData.AsEnumerable().Where(x => double.TryParse(x.Field<string>("EV/EBIT"), out double res) == true).Max(x => Convert.ToDouble(x.Field<string>("EV/EBIT")));

            string land = outputRow["Land"].ToString();
            double.TryParse(outputRow["P/E"].ToString(), out double PE);
            double.TryParse(outputRow["P/S"].ToString(), out double PS);
            double.TryParse(outputRow["P/B"].ToString(), out double PB);
            double.TryParse(outputRow["P/FCF"].ToString(), out double PFCF);
            double.TryParse(outputRow["P/EBITDA"].ToString(), out double PEBITDA);
            double.TryParse(outputRow["EV/EBIT"].ToString(), out double EBIT);

            if (PE <= 0)
            {
                outputRow["P/E"] = highestPE;
            }

            if (PS <= 0)
            {
                outputRow["P/S"] = highestPS;
            }

            if (PB <= 0)
            {
                outputRow["P/B"] = highestPB;
            }

            if (PFCF <= 0)
            {
                outputRow["P/FCF"] = highestPFCF;
            }

            if (PEBITDA <= 0)
            {
                outputRow["P/EBITDA"] = highestPEBITDA;
            }

            if (EBIT <= 0)
            {
                outputRow["EV/EBIT"] = highestEBIT;
            }

            if (land == "Finland")
            {
                int.TryParse(outputRow["Börsvärde"].ToString(), out int EV);
                outputRow["Börsvärde"] = EV * 10;
            }

            return outputRow;
        }

        private DataTable[] CreateEmptyResultTable(DataTable saniData)
        {
            DataTable[] tbl = new DataTable[2];
            tbl[0] = saniData.Clone(); //VC
            tbl[1] = saniData.Clone(); //EV/EBIT

            #region VC
            List<string> KeepColumnsList = new List<string>() { "Bolagsnamn", "P/E", "P/S", "P/B", "P/FCF", "P/EBITDA" };
            List<DataColumn> remove = new List<DataColumn>();
            foreach (DataColumn item in tbl[0].Columns)
            {
                if (!KeepColumnsList.Contains(item.ColumnName))
                {
                    remove.Add(item);
                }
            }

            foreach (DataColumn item in remove)
            {
                tbl[0].Columns.Remove(item);
            }

            tbl[0].Columns.Add(new DataColumn() { ColumnName = "Momentum Rank", DefaultValue = 0, DataType = typeof(string) });
            tbl[0].Columns.Add(new DataColumn() { ColumnName = "VC Rank", DefaultValue = 0, DataType = typeof(string) });
            tbl[0].Columns.Add(new DataColumn() { ColumnName = "Total VC + MoM", DefaultValue = 0, DataType = typeof(string) });
            tbl[0].Columns.Add(new DataColumn() { ColumnName = "VC + MoM Rank", DefaultValue = 0, DataType = typeof(string) });
            tbl[0].Columns.Add(new DataColumn() { ColumnName = "Delete", DefaultValue = 0, DataType = typeof(string) });
            #endregion

            #region EV/EBIT
            KeepColumnsList = new List<string>() { "Bolagsnamn" };
            remove = new List<DataColumn>();
            foreach (DataColumn item in tbl[1].Columns)
            {
                if (!KeepColumnsList.Contains(item.ColumnName))
                {
                    remove.Add(item);
                }
            }

            foreach (DataColumn item in remove)
            {
                tbl[1].Columns.Remove(item);
            }

            tbl[1].Columns.Add(new DataColumn() { ColumnName = "EV/EBIT", DefaultValue = 0, DataType = typeof(string) });
            tbl[1].Columns.Add(new DataColumn() { ColumnName = "Momentum", DefaultValue = 0, DataType = typeof(string) });
            tbl[1].Columns.Add(new DataColumn() { ColumnName = "Rank", DefaultValue = 0, DataType = typeof(string) });
            tbl[1].Columns.Add(new DataColumn() { ColumnName = "Delete", DefaultValue = 0, DataType = typeof(string) });

            //tbl[1].Columns.Add(new DataColumn() { ColumnName = "EV/EBIT Rank", DefaultValue = 0, DataType = typeof(string) });
            //tbl[1].Columns.Add(new DataColumn() { ColumnName = "Momentum Rank", DefaultValue = 0, DataType = typeof(string) });
            //tbl[1].Columns.Add(new DataColumn() { ColumnName = "Total EV/EBIT + MoM", DefaultValue = 0, DataType = typeof(string) });
            //tbl[1].Columns.Add(new DataColumn() { ColumnName = "EV/EBIT + MoM Rank", DefaultValue = 0, DataType = typeof(string) });
            //tbl[1].Columns.Add(new DataColumn() { ColumnName = "Delete", DefaultValue = 0, DataType = typeof(string) });
            #endregion

            return tbl;
        }

        private DataTable ResultsViewVC(DataTable saniData)
        {
            DataTable tbl = saniData.Copy();
            tbl.Columns.Add(new DataColumn() { ColumnName = "Total Momentum", DefaultValue = 0, DataType = typeof(string) });
            tbl.Columns.Add(new DataColumn() { ColumnName = "Momentum Rank", DefaultValue = 0, DataType = typeof(string) });
            tbl.Columns.Add(new DataColumn() { ColumnName = "Total VC", DefaultValue = 0, DataType = typeof(string) });
            tbl.Columns.Add(new DataColumn() { ColumnName = "VC Rank", DefaultValue = 0, DataType = typeof(string) });
            tbl.Columns.Add(new DataColumn() { ColumnName = "Total VC + MoM", DefaultValue = 0, DataType = typeof(string) });
            tbl.Columns.Add(new DataColumn() { ColumnName = "VC + MoM Rank", DefaultValue = 0, DataType = typeof(string) });

            List<double> peList = tbl.Rows.OfType<DataRow>().Select(dr => double.Parse(dr.Field<string>("P/E"))).ToList();
            List<double> psList = tbl.Rows.OfType<DataRow>().Select(dr => double.Parse(dr.Field<string>("P/S"))).ToList();
            List<double> pbList = tbl.Rows.OfType<DataRow>().Select(dr => double.Parse(dr.Field<string>("P/B"))).ToList();
            List<double> ebitdaList = tbl.Rows.OfType<DataRow>().Select(dr => double.Parse(dr.Field<string>("P/EBITDA"))).ToList();
            List<double> fcfList = tbl.Rows.OfType<DataRow>().Select(dr => double.Parse(dr.Field<string>("P/FCF"))).ToList();

            foreach (DataRow item in saniData.Rows)
            {
                double.TryParse(item["P/E"].ToString(), out double PE);
                double.TryParse(item["P/S"].ToString(), out double PS);
                double.TryParse(item["P/B"].ToString(), out double PB);
                double.TryParse(item["P/FCF"].ToString(), out double PFCF);
                double.TryParse(item["P/EBITDA"].ToString(), out double PEBITDA);
                double.TryParse(item["Utveck.  6m"].ToString().Replace("%", ""), out double mom6);
                double.TryParse(item["Utveck.  1 år"].ToString().Replace("%", ""), out double mom12);

                double adjustedMoM6 = 100 / (1 + (mom6 / 100));
                double adjustedMoM12 = 100 / (1 + (mom12 / 100));
                double MoM6to12 = (adjustedMoM6 / adjustedMoM12) - 1;
                double adjustesMoM6to12 = Math.Round(MoM6to12, 3);
                double adjustesMoM0to6 = mom6 / 100;
                double totalMomentum = ((adjustesMoM6to12 + adjustesMoM0to6) / 2.0) * 100;

                double pePercent = Math.Abs((Math.Round(PercentRank(peList, PE), 3) * 100) - 100);
                double pbPercent = Math.Abs((Math.Round(PercentRank(pbList, PB), 3) * 100) - 100);
                double psPercent = Math.Abs((Math.Round(PercentRank(psList, PS), 3) * 100) - 100);
                double ebitdaPercent = Math.Abs((Math.Round(PercentRank(ebitdaList, PEBITDA), 3) * 100) - 100);
                double fcfPercent = Math.Abs((Math.Round(PercentRank(fcfList, PFCF), 3) * 100) - 100);

                string name = item[0].ToString();
                string nation = item[1].ToString();
                DataRow row = tbl.Select().Where(x => (x[0].ToString() == name) && (x[1].ToString() == nation)).FirstOrDefault();

                if (row != null)
                {
                    row["P/E"] = (pePercent.ToString() + "%").Replace(",", ".");
                    row["P/S"] = (psPercent.ToString() + "%").Replace(",", ".");
                    row["P/B"] = (pbPercent.ToString() + "%").Replace(",", ".");
                    row["P/FCF"] = (fcfPercent.ToString() + "%").Replace(",", ".");
                    row["P/EBITDA"] = (ebitdaPercent.ToString() + "%").Replace(",", ".");
                    row["Total Momentum"] = Math.Round(totalMomentum, 2);
                    row["Total VC"] = ((pePercent + psPercent + pbPercent + ebitdaPercent + fcfPercent) / 5).ToString();
                }
            }

            List<double> momList = tbl.Rows.OfType<DataRow>().Select(dr => double.Parse(dr.Field<string>("Total Momentum"))).ToList();
            List<double> totalList = tbl.Rows.OfType<DataRow>().Select(dr => double.Parse(dr.Field<string>("Total VC + MoM"))).ToList();
            List<double> vcList = tbl.Rows.OfType<DataRow>().Select(dr => double.Parse(dr.Field<string>("Total VC"))).ToList();

            foreach (DataRow item in tbl.Rows)
            {
                double.TryParse(item["Total Momentum"].ToString(), out double mom);
                double.TryParse(item["Total VC"].ToString(), out double totVC);

                double momPercent = Math.Abs(Math.Round(PercentRank(momList, mom), 3) * 100);
                double vcPercent = Math.Abs(Math.Round(PercentRank(vcList, totVC), 3) * 100);

                item["Momentum Rank"] = momPercent;
                item["VC Rank"] = vcPercent;
                item["Total VC + MoM"] = Math.Round((momPercent + totVC) / 2.0, 3);
            }

            List<double> totalRankList = tbl.Rows.OfType<DataRow>().Select(dr => double.Parse(dr.Field<string>("Total VC + MoM"))).ToList();

            foreach (DataRow item in tbl.Rows)
            {
                double.TryParse(item["Total VC + MoM"].ToString(), out double tot);
                int rank = GetRank(tot, totalRankList);
                item["VC + MoM Rank"] = rank;

                item["Momentum Rank"] = item["Momentum Rank"].ToString().Replace(",", ".") + "%";
                item["VC Rank"] = item["VC Rank"].ToString().Replace(",", ".") + "%";
                item["Total VC + MoM"] = item["Total VC + MoM"].ToString().Replace(",", ".") + "%";
                item["Total Momentum"] = item["Total Momentum"].ToString().Replace(",", ".") + "%";
                item["Total VC"] = item["Total VC"].ToString().Replace(",", ".") + "%";
            }

            tbl.Columns.Remove(tbl.Columns["Total VC"]);
            tbl.Columns.Remove(tbl.Columns["Utveck.  1 år"]);
            tbl.Columns.Remove(tbl.Columns["Utveck.  6m"]);
            tbl.Columns.Remove(tbl.Columns["Utveck.  3m"]);
            tbl.Columns.Remove(tbl.Columns["Medelvärde"]);
            tbl.Columns.Remove(tbl.Columns["Sen. Rapport"]);
            tbl.Columns.Remove(tbl.Columns["Total Momentum"]);
            tbl.Columns.Remove(tbl.Columns["Land"]);
            tbl.Columns.Remove(tbl.Columns["Börsvärde"]);
            tbl.Columns.Remove(tbl.Columns["EV/EBIT"]);

            return tbl;
        }

        private DataTable ResultViewEBIT(DataTable saniData, int EBITratio)
        {
            DataTable tbl = saniData.Copy();
            List<double> EBITList = tbl.Rows.OfType<DataRow>().Select(dr => double.Parse(dr.Field<string>("EV/EBIT"))).ToList();
            List<int> removeAt = new List<int>();

            EBITList.Sort();
            double realRatio = EBITList.Count * (EBITratio / (double)100);
            int cutOfInt = (int)Math.Round(realRatio);
            double cutOfValue = EBITList[cutOfInt];

            foreach (DataRow item in saniData.Rows)
            {
                double.TryParse(item["EV/EBIT"].ToString(), out double EBIT);

                if (EBIT > cutOfValue)
                {
                    removeAt.Add(saniData.Rows.IndexOf(item));
                }
            }

            removeAt.Sort();
            removeAt.Reverse();
            foreach (int index in removeAt)
            {
                tbl.Rows.RemoveAt(index);
            }

            tbl.Columns.Add(new DataColumn() { ColumnName = "Momentum", DefaultValue = 0, DataType = typeof(string) });
            tbl.Columns.Add(new DataColumn() { ColumnName = "Rank", DefaultValue = 0, DataType = typeof(string) });
            //tbl.Columns.Add(new DataColumn() { ColumnName = "EV/EBIT Rank", DefaultValue = 0, DataType = typeof(string) });
            //tbl.Columns.Add(new DataColumn() { ColumnName = "Total EV/EBIT + MoM", DefaultValue = 0, DataType = typeof(string) });
            //tbl.Columns.Add(new DataColumn() { ColumnName = "EV/EBIT + MoM Rank", DefaultValue = 0, DataType = typeof(string) });

            //List<double> EBITList = tbl.Rows.OfType<DataRow>().Select(dr => double.Parse(dr.Field<string>("EV/EBIT"))).ToList();

            foreach (DataRow item in tbl.Rows)
            {
                double.TryParse(item["Utveck.  6m"].ToString().Replace("%", ""), out double mom6);
                double.TryParse(item["Utveck.  1 år"].ToString().Replace("%", ""), out double mom12);

                double adjustedMoM6 = 100 / (1 + (mom6 / 100));
                double adjustedMoM12 = 100 / (1 + (mom12 / 100));
                double MoM6to12 = (adjustedMoM6 / adjustedMoM12) - 1;
                double adjustesMoM6to12 = Math.Round(MoM6to12, 3);
                double adjustesMoM0to6 = mom6 / 100;
                double totalMomentum = ((adjustesMoM6to12 + adjustesMoM0to6) / 2.0) * 100;

                string name = item[0].ToString();
                string nation = item[1].ToString();
                DataRow row = tbl.Select().Where(x => (x[0].ToString() == name) && (x[1].ToString() == nation)).FirstOrDefault();

                if (row != null)
                {
                    //row["Total Momentum"] = Math.Round(totalMomentum, 2).ToString().Replace(",", ".") + "%";
                    row["Momentum"] = Math.Round(totalMomentum, 2); //.ToString().Replace(",", ".") + "%";                
                }
            }

            //List<double> momList = tbl.Rows.OfType<DataRow>().Select(dr => double.Parse(dr.Field<string>("Total Momentum"))).ToList();
            //List<double> totalList = tbl.Rows.OfType<DataRow>().Select(dr => double.Parse(dr.Field<string>("Total EV/EBIT + MoM"))).ToList();
            //List<double> ebitList = tbl.Rows.OfType<DataRow>().Select(dr => double.Parse(dr.Field<string>("EV/EBIT"))).ToList();

            //foreach (DataRow item in tbl.Rows)
            //{
            //    double.TryParse(item["Total Momentum"].ToString(), out double mom);
            //    double.TryParse(item["EV/EBIT"].ToString(), out double totEbit);

            //    double momPercent = Math.Abs(Math.Round(PercentRank(momList, mom), 3) * 100);
            //    double ebitPercent = Math.Abs((Math.Round(PercentRank(ebitList, totEbit), 3) * 100) - 100);

            //    item["Momentum Rank"] = momPercent;
            //    item["EV/EBIT Rank"] = ebitPercent;
            //    item["Total EV/EBIT + MoM"] = Math.Round((momPercent + ebitPercent) / 2.0, 3);
            //}

            List<double> totalRankList = tbl.Rows.OfType<DataRow>().Select(dr => double.Parse(dr.Field<string>("Momentum"))).ToList();

            foreach (DataRow item in tbl.Rows)
            {
                double.TryParse(item["Momentum"].ToString(), out double tot);
                int rank = GetRank(tot, totalRankList);
                item["Rank"] = rank;

                item["Momentum"] = item["Momentum"].ToString().Replace(",", ".") + "%";
                //item["EV/EBIT Rank"] = item["EV/EBIT Rank"].ToString().Replace(",", ".") + "%";
                //item["Total EV/EBIT + MoM"] = item["Total EV/EBIT + MoM"].ToString().Replace(",", ".") + "%";
                //item["Total Momentum"] = item["Total Momentum"].ToString().Replace(",", ".") + "%";
                //item["EV/EBIT"] = item["EV/EBIT"].ToString().Replace(",", ".") + "%";
            }

            tbl.Columns.Remove(tbl.Columns["Utveck.  1 år"]);
            tbl.Columns.Remove(tbl.Columns["Utveck.  6m"]);
            tbl.Columns.Remove(tbl.Columns["Utveck.  3m"]);
            tbl.Columns.Remove(tbl.Columns["Medelvärde"]);
            tbl.Columns.Remove(tbl.Columns["Sen. Rapport"]);
            //tbl.Columns.Remove(tbl.Columns["Total Momentum"]);
            tbl.Columns.Remove(tbl.Columns["Land"]);
            tbl.Columns.Remove(tbl.Columns["Börsvärde"]);
            tbl.Columns.Remove(tbl.Columns["P/EBITDA"]);
            tbl.Columns.Remove(tbl.Columns["P/FCF"]);
            tbl.Columns.Remove(tbl.Columns["P/E"]);
            tbl.Columns.Remove(tbl.Columns["P/B"]);
            tbl.Columns.Remove(tbl.Columns["P/S"]);
            //tbl.Columns.Remove(tbl.Columns["EV/EBIT"]);

            return tbl;
        }

        private static double PercentRank(List<double> matrix, double value)
        {
            matrix.Sort();

            for (int i = 0; i < matrix.Count; i++)
                if (matrix[i] == value)
                    return ((double)i) / (matrix.Count - 1);

            // calculate value using linear interpolation
            double x1, x2, y1, y2;

            for (int i = 0; i < matrix.Count - 1; i++)
            {
                if (matrix[i] < value && value < matrix[i + 1])
                {
                    x1 = matrix[i];
                    x2 = matrix[i + 1];
                    y1 = PercentRank(matrix, x1);
                    y2 = PercentRank(matrix, x2);

                    return (((x2 - value) * y1 + (value - x1) * y2)) / (x2 - x1);
                }
            }

            throw new Exception("Out of bounds");
        }

        private int GetRank(double value, List<double> totalRankList)
        {
            int rank = 1;

            foreach (double item in totalRankList)
            {
                if (value < item)
                {
                    rank++;
                }
            }

            return rank;
        }

        private DataTable ConvertToDataTable<T>(IList<T> data)
        {
            PropertyDescriptorCollection properties = TypeDescriptor.GetProperties(typeof(T));
            DataTable table = new DataTable();

            foreach (PropertyDescriptor prop in properties)
            {
                table.Columns.Add(prop.Name, Nullable.GetUnderlyingType(prop.PropertyType) ?? prop.PropertyType);
            }

            foreach (T item in data)
            {
                DataRow row = table.NewRow();
                foreach (PropertyDescriptor prop in properties)
                    row[prop.Name] = prop.GetValue(item) ?? DBNull.Value;
                table.Rows.Add(row);
            }

            return table;

        }
    }
}
