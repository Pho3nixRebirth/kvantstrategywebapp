﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;

namespace KvantStrategi.Models
{
    public class DatatableObjects
    {
        public DataTable SanitizedDataTable { get; set; }
        public DataTable RankedDataTable { get; set; }
        public DataTable RankedEBITDataTable { get; set; }
    }

    public class DataTableJson
    {
        [JsonProperty(PropertyName = "Bolagsnamn")]
        public string Bolagsnamn { get; set; }

        [JsonProperty(PropertyName = "Land")]
        public string Land { get; set; }

        [JsonProperty(PropertyName = "Börsvärde")]
        public string Börsvärde { get; set; }

        [JsonProperty("P/E")]
        public string PE { get; set; }

        [JsonProperty(PropertyName = "P/S")]
        public string PS { get; set; }

        [JsonProperty(PropertyName = "P/B")]
        public string PB { get; set; }

        [JsonProperty(PropertyName = "P/FCF")]
        public string FCF { get; set; }

        [JsonProperty(PropertyName = "P/EBITDA")]
        public string EBITDA { get; set; }

        [JsonProperty(PropertyName = "EV/EBIT")]
        public string EBIT { get; set; }

        [JsonProperty(PropertyName = "Utveck.  1 år")]
        public string Mom12 { get; set; }

        [JsonProperty(PropertyName = "Utveck.  6m")]
        public string Mom6 { get; set; }

        [JsonProperty(PropertyName = "Utveck.  3m")]
        public string Mom3 { get; set; }

        [JsonProperty(PropertyName = "Medelvärde")]
        public string MA { get; set; }

        [JsonProperty(PropertyName = "Sen. Rapport")]
        public string Rapport { get; set; }
    }

    public enum ResponsType
    {
        VC,
        EBIT
    }
}
